import pandas as pd
from Wrappers.QuandlWrapper import getHistoricData
from Utilities.ConstituentUtilities import GetBenchmarkConstituents
import logging
from os.path import exists
    
def GetInstrumentPriceMatrix(instruments, startDate, endDate, column):
    return _createInstrumentPriceMatrix(instruments, startDate, endDate, column)

def GetBenchmarkPriceMatrix(startDate, endDate, column):
    logging.info("Using benchmark {} Matrix!".format(column))
    picklePath = "Data/Cache/Benchmark_{}_{}_{}Matrix".format(startDate,endDate,column.replace(" ", ""))
    if exists(picklePath):
        return pd.read_pickle(picklePath)
    else:
        instruments = GetBenchmarkConstituents().index.values
        matrix = _createInstrumentPriceMatrix(instruments, startDate, endDate, column)
        matrix.to_pickle(picklePath)
        return matrix

def _createInstrumentPriceMatrix(instruments, startDate, endDate, column):
    tickers = []
    columns = []

    for ticker in instruments:
        data = getHistoricData(ticker, startDate, endDate, columns=[column])[column]
        if data.empty:
            logging.debug("No data found for {} between {} and {}".format(ticker, startDate, endDate))
        else:
            columns.append(data)
            tickers.append(ticker)

    return pd.concat(columns, axis=1, keys=tickers)

def GetBenchmarkTrainingData():
    picklePath = "Data/TrainingData.pickle"
    if exists(picklePath):
        return pd.read_pickle(picklePath)
    else:
        data = GetBenchmarkData("Data/TrainingData.xlsx")
        data.to_pickle(picklePath)
        return data

def GetBenchmarkTestingData():
    picklePath = "Data/TestingData.pickle"
    if exists(picklePath):
        return pd.read_pickle(picklePath)
    else:
        data = GetBenchmarkData("Data/TestingData.xlsx")
        data.to_pickle(picklePath)
        return data

def GetBenchmarkData(datafile):
    return pd.read_excel(datafile, header=0, index_col=0)