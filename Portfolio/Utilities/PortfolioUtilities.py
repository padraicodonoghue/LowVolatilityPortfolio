from Utilities.ConstituentUtilities import GetBenchmarkConstituents
from Utilities.PortfolioMeasurements import Normalize
import pandas as pd

def MergePortfolioWithBenchmark(portfolio):
    """ Join instruments in portfolio to those present in the benchmark """
    return MergePortfolioWithInstruments(GetBenchmarkConstituents(), portfolio)

def MergePortfolioWithInstruments(instruments, portfolio):
    """ Join portfolio with list of instruments """
    joint = instruments.join(portfolio["Weight"])
    joint["Weight"].fillna(0, inplace=True)
    return joint

def MergeInstrumentsWithWeights(instruments, weights):
    portfolio = pd.DataFrame(Normalize(weights), columns=["Weight"], index=instruments)
    portfolio["Weight"].fillna(0, inplace=True)
    return portfolio

def PostProcessResults(instruments, results, details):
    details["Score"] = results.fun
    return MergeInstrumentsWithWeights(instruments, results.x), details