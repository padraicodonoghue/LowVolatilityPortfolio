import numpy as np
import pandas as pd
import logging

def Variance(weights, matrix):
    """ Computes the variance of a portfolio """
    val = weights.dot(matrix).dot(weights.transpose())
    logging.debug("Volatility: {}".format(val))
    return val

def NormalizedVariance(weights, matrix):
    """ Normalizes a portfolio before computing its variance """
    return Variance(Normalize(weights), matrix)

def VarianceDerivative(weights, matrix):
    """ Computes the partial first order derivatives of the portfolio """
    res = 2*weights.dot(matrix)
    return res

def NormalizedVarianceDerivative(weights, matrix):
    """ Normalizes a portfolio before computing its partial first order derivatives """
    return VarianceDerivative(Normalize(weights), matrix)

def Normalize(weights):
    """ Normalizes a portfolio so that its weights sum to 1 """
    total = np.sum(weights)
        
    if (total == 0):
        return weights + 1 / weights.size
        
    return weights / total

def HistoricVolatility(weights, history):
    """ Computes the historic volatility of a portfolio"""
    val = ((history * weights).sum(axis=1)/weights.sum()).std()
    logging.debug("Historic Volatility: {}".format(val))
    return val

def NonZeroMin(x):
    min = np.ma.masked_equal(x, 0.0, copy=False).min()
    if (min == np.nan):
        raise ArithmeticError
    logging.debug("NonZeroMin: {}".format(min))
    return min
