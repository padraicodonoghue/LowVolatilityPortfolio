import pandas as pd
import logging

def GetSP500ConstituentsByYear(year):
    return GetConstituentsByYear("Data/SP500YearlyConstituents.xlsx", year)

def GetSP100ConstituentsByYear(year):
    return GetConstituentsByYear("Data/SP100YearlyConstituents.xlsx", year)

def GetConstituentsByYear(datafile, year):
    """ Returns a DataFrame containing the constituents of the S&P 500/100 at the start of a given year."""

    constituents = pd.read_excel(datafile, header=0, index_col=0)

    if type(year) != str:
        year = str(year)

    if year not in constituents.columns.values:
        raise Exception("Year outside range 2016-2008")    

    # Select instruments 
    constituents = constituents.loc[constituents[str(year)] == 'X']
   
    logging.debug("Retrieved {} constituents from S&P 500 {}".format(len(constituents.index.values), year))
    return constituents[["Company"]]

def GetBenchmarkConstituents():
    return pd.read_excel("Data/BenchmarkConstituents.xlsx", header=0, index_col=0)

def GetBenchmarkLargeCapConstituents():
    return pd.read_excel("Data/BenchmarkLargeCapConstituents.xlsx", header=0, index_col=0)
