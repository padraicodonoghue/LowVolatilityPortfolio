import pandas as pd
from datetime import datetime
from os import makedirs, path
from Utilities.PortfolioUtilities import MergePortfolioWithBenchmark

def MakeResultsDirectory(name=None):
    """ Ensure results directory exists. name defaults to current DateTime """
    if name == None:
        name = datetime.utcnow().strftime('%Y-%m-%d-T-%H-%M-%S')
    resultsFolder = path.join("Portfolios/", name)
    makedirs(resultsFolder, exist_ok=True)
    return resultsFolder

def OutputDetails(file, details):
    """ Write the contents of detail to a file """
    with open(file, 'x') as f:
        for key, value in details.items():
            f.write("{}: {} \n".format(key, value))

def BenchmarkOutput(file, portfolio):
    MergePortfolioWithBenchmark(portfolio).to_excel(file)