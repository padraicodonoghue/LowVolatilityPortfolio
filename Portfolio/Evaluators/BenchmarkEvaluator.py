""" Functions for evaluation a portfolios historic data against the benchmark """
from math import sqrt
from Utilities.DataUtilities import GetBenchmarkTrainingData, GetBenchmarkTestingData
from Utilities.PortfolioUtilities import MergePortfolioWithBenchmark
from Utilities.PortfolioMeasurements import HistoricVolatility

def BenchmarkPortfolioAgainstTraining(portfolio):
    return BenchmarkPortfolio(portfolio, GetBenchmarkTrainingData())

def BenchmarkPortfolioAgainstTesting(portfolio):
    return BenchmarkPortfolio(portfolio, GetBenchmarkTestingData())

def BenchmarkPortfolio(portfolio, history):
    weights = portfolio["Weight"].values
    results = {}
    volatility = HistoricVolatility(weights, history)
    results["Volatility"] = volatility
    results["Annulaized Volatility"] = volatility * sqrt(history.shape[0])
    return results

def GetBenchmarkResults(portfolio):
    portfolio = MergePortfolioWithBenchmark(portfolio)
    training = GetBenchmarkTrainingData()
    testing = GetBenchmarkTestingData()

    trainingVol = BenchmarkPortfolio(portfolio, training)
    testingVol = BenchmarkPortfolio(portfolio, testing)
    results = {"Training Volatility" : trainingVol["Volatility"],
               "Training Annualized Volatility" : trainingVol["Annulaized Volatility"],
               "Testing Volatility" : testingVol["Volatility"],
               "Testing Annualized Volatility" : testingVol["Annulaized Volatility"]}

    #results["Training Annualized Volatility"] = trainingVol * sqrt(training.shape[0])
    #results["Testing Annualized Volatility"] = testingVol * sqrt(testing.shape[0])

    return results
