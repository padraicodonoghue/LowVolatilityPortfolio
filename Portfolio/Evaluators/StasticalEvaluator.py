def GetPortfolioStatistics(portfolio):
    stats = {}
    portfolio = portfolio[portfolio["Weight"] != 0]
    stats["No of Stocks"] = portfolio.shape[0]
    stats["Mean Weight"] = portfolio["Weight"].mean()

    return stats