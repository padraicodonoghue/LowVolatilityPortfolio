from DataPrepper import GetInstrumentPriceMatrix
import pandas as pd
from os.path import exists

def GetBacktestData(instruments, backtestStartDate, backtestEndDate, benchmark=False):
    # Require 95% of values to be present to use date, forward fill any remaining missing dates and backfill to 
    # fill dates missing at the start.

    if benchmark:
        picklePath = "Data/Cache/BenchmarkBacktestData"
        if exists(picklePath):
            return pd.read_pickle(picklePath)
        else:
            data = CalculateBacktestData(instruments, backtestStartDate, backtestEndDate, benchmark)
            data.to_pickle(picklePath)
            return data
    return CalculateBacktestData(instruments, backtestStartDate, backtestEndDate)

def CalculateBacktestData(instruments, backtestStartDate, backtestEndDate, benchmark):
    return GetInstrumentPriceMatrix(instruments, backtestStartDate, backtestEndDate, "Close", benchmark) \
                   .dropna(thresh = 0.95 * len(instruments)) \
                   .fillna(method="ffill") \
                   .fillna(method="bfill")

def EvaluatePortfolio(portfolio, backtestStartDate, backtestEndDate, benchmark=False):
    """ Evaluate a portfolios performance between two dates """
    data = GetBacktestData(portfolio.index.values, backtestStartDate, backtestEndDate, benchmark)

    prices = data[:data.first_valid_index()]

    shares = portfolio["Invested"] / prices

    portfolio["Shares"] = shares.T

    instrumentValues = (data * portfolio["Shares"]).T

    portfolioValues = pd.DataFrame(instrumentValues.sum(), columns=["Value"])

    performanceVolatility = portfolioValues["Value"].pct_change().var()

    return instrumentValues, portfolioValues, performanceVolatility