import timeit
from os import path
from Evaluators.BenchmarkEvaluator import GetBenchmarkResults
from Evaluators.StasticalEvaluator import GetPortfolioStatistics
from Reporters.Reporter import MakeResultsDirectory, OutputDetails, BenchmarkOutput

def RunEvaluation(function, outputDir = None):
    """ Takes a function that optimizes a portfolio and returns the optimized portfolio 
        and its score. """
    portfolio, details = TimeFunction(function)

    evaluation = EvaluatePortfolio(portfolio)

    details.update(evaluation)

    SaveReport(portfolio, details, outputDir)

    details["Portfolio"] = portfolio

    return details

def TimeFunction(function):
    """ Runs an optimization and returns the portfolio, score and seconds taken to run. """
    start = timeit.default_timer()

    result = function()

    stop = timeit.default_timer()

    seconds = stop - start
    
    result[1]["Time"] = seconds

    return result[0], result[1]

def EvaluatePortfolio(portfolio):
    evaluation = {}

    benchmarkResults = GetBenchmarkResults(portfolio)

    evaluation.update(benchmarkResults)
   
    stats = GetPortfolioStatistics(portfolio)

    evaluation.update(stats)

    return evaluation

def SaveReport(portfolio, details, outputDir):
    folder = MakeResultsDirectory(outputDir)

    portfolio.to_excel(path.join(folder, "Portfolio.xlsx"))
    BenchmarkOutput(path.join(folder, "Benchmark.xlsx"), portfolio)
    OutputDetails(path.join(folder, "Summary.txt"), details)
    