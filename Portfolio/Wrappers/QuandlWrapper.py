import Quandl
import pandas as pd
import logging
from os.path import isfile
_authToken = "ZzezjpzYpFzNysNXLzww"

def getHistoricData(ticker, startDate, endDate, columns=["Open", "High", "Low", "Close", "Volume", "Adjusted Close"]):
    with pd.HDFStore("Data/Cache/Quandl.h5") as store:
        if ticker in store:
            logging.debug("Cache Hit: {}".format(ticker))
        else:
            logging.debug("Cache Miss: {}".format(ticker))
            try:
                data = Quandl.get("YAHOO/"+ticker, authtoken=_authToken, trim_start="2000-01-01")
                store.put(ticker, data, format="table")
                logging.debug("Cache Filled: {}".format(ticker))
            except Quandl.DatasetNotFound:
                logging.debug("Dataset not in YAHOO: {}".format(ticker))
                return pd.DataFrame(columns=columns)
        return store.select(ticker, [pd.Term("index", ">=", pd.Timestamp(startDate)), 
                              pd.Term("index", "<=", pd.Timestamp(endDate))], columns=columns)

