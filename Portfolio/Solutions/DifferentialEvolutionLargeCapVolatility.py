from Utilities.ConstituentUtilities import GetBenchmarkLargeCapConstituents
from Utilities.PortfolioUtilities import PostProcessResults
from Utilities.DataUtilities import GetBenchmarkTrainingData
from Utilities.PortfolioMeasurements import HistoricVolatility
from scipy.optimize import differential_evolution
import numpy as np
import Utilities

details =  {"Constitiuents" : "Benchmark",
           "Volatility Measure" : "Covariance",
           "Jacobian" : False,
           "Optimization": "Differential Evolution",
           "Training Data" : "Benchmark"}

def GeneratePortfolio():
    instruments = GetBenchmarkLargeCapConstituents()

    history = GetBenchmarkTrainingData()

    viableInsts = instruments.index.intersection(history.columns)

    history = history[viableInsts]

    bounds = tuple([(0, 100000) for x in range(viableInsts.shape[0])])  

    minimizeFunction = lambda weights : HistoricVolatility(weights, history)

    return PostProcessResults(
        viableInsts,                                                    
        differential_evolution(func=minimizeFunction,
                               bounds=bounds,
                               disp=True,
                               polish=True),
        details)