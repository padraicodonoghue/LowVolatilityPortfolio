from Utilities.ConstituentUtilities import GetBenchmarkConstituents
from Utilities.PortfolioUtilities import PostProcessResults
from Utilities.DataUtilities import GetBenchmarkPriceMatrix
from Utilities.PortfolioMeasurements import NormalizedVariance, NormalizedVarianceDerivative
from scipy.optimize import differential_evolution
import numpy as np
import Utilities

details =  {"Constitiuents" : "Benchmark 10 Least Volatile",
           "Volatility Measure" : "Covariance",
           "Jacobian" : False,
           "Optimization": "Differential Evolution",
           "Training Start": "2008-01-03",
           "Training End": "2011-06-30",
           "Historic Column": "Close",
           "Data Transformation": "Percentage Change"}

def GeneratePortfolio():
    instruments = GetBenchmarkConstituents()

    historicData = GetBenchmarkPriceMatrix(details["Training Start"], 
                                           details["Training End"], 
                                           details["Historic Column"]).pct_change()

    volatilities = historicData.var().sort_values()

    candidates = volatilities.head(10)

    covMatrix = historicData[candidates.index].cov()

    bounds = tuple([(0, 0.2) for x in range(covMatrix.shape[0])])

    minimizeFunction = lambda weights : NormalizedVariance(weights, covMatrix)

    return PostProcessResults(
        covMatrix.index.values,                                                    
        differential_evolution(func=minimizeFunction,
                               bounds=bounds,
                               maxiter=10000,
                               popsize=20,
                               disp=True,
                               polish=True),
        details)