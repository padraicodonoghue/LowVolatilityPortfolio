from Utilities.ConstituentUtilities import GetBenchmarkConstituents
from Utilities.PortfolioUtilities import PostProcessResults
from Utilities.DataUtilities import GetBenchmarkTrainingData
from Utilities.PortfolioMeasurements import HistoricVolatility, Normalize
from scipy.optimize import basinhopping
import numpy as np
import Utilities


details = {"Constitiuents" : "Benchmark 50 Least Var",
           "Volatility Measure" : "Historic Volatility",
           "Jacobian" : True,
           "Optimization": "Basinhopping (100 Iters, Stepsize = 5, T=5) + SLSQP + Constriants",
           "Starting Weight": "1/Vol",
           "Training Start": "2008-01-03",
           "Training End": "2011-06-30",
           "Historic Column": "Close",
           "Data Transformation": "Percentage Change"}

def GeneratePortfolio():
    instruments = GetBenchmarkConstituents()

    historicData = GetBenchmarkTrainingData()

    leastVolatile = historicData.var().sort_values()

    viableInsts = leastVolatile.index.intersection(historicData.columns)

    historicData = historicData[viableInsts]

    minimizeFunction = lambda weights : HistoricVolatility(weights, historicData)

    initialWeights = np.array([0,0,0,0,0.65264744,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.524827805,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.583116274,0,0,0,0.678591842,0.642535552,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.621005967,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.340017756,0.629332533,0,0.71319872,0,0,0,0,0.524610231,0,0,0,0,0,0,0,0,0.506707873,0.686851642,0,0,0,0,0,0,0,0,0,0,0,0,0.597785792,0,0,0,0.342138496,0,0.549357261,0,0,0,0,0.577611724,0,0,0,0,0,0,0,0.609919605,0,0.514314283,0,0,0,0.730243699,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.514540378,0,0,0,0,0,0,0.368324519,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.70562659,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.664498342,0,0,0,0,0,0,0.69495874,0,0,0,0.544469535,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.746084427,0,0,0,0,0.676009245,0,0.621413151,0,0.69028042,0,0,0.516156166,0,0,0,0,0.654240662,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.631456559,0,0,0,0,0,0.710329578,0,0,0.604853525,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.29984609,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.655687488,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.591868796,0,0,0,0,0,0,0,0,0,0,0,0,0,0.63266714,0,0,0,0,0,0,0,0,0.593731859,0,0,0,0,0.707149109,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.505849768,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.69923484,0,0,0,0,0,0,0,0.645751395,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
    bounds = tuple([(0, None) for x in range(historicData.shape[1])])

    return PostProcessResults(historicData.columns.values,
                              basinhopping(func=minimizeFunction,
                              x0= initialWeights,
                              minimizer_kwargs = {
                                  "method":"SLSQP",
                                  #"constraints": {'type': 'eq', 'fun': lambda x:  x.sum() - 1},
                                  "options" : {
                                              "disp": False
                                          }, 
                                  "bounds": bounds},
                              disp=True
                              ),
                              details)