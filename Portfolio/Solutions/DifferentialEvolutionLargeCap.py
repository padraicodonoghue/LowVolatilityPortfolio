from Utilities.ConstituentUtilities import GetBenchmarkLargeCapConstituents
from Utilities.PortfolioUtilities import PostProcessResults
from Utilities.DataUtilities import GetBenchmarkPriceMatrix
from Utilities.PortfolioMeasurements import NormalizedVariance, NormalizedVarianceDerivative
from scipy.optimize import differential_evolution
import numpy as np
import Utilities

details =  {"Constitiuents" : "Benchmark Large Cap",
           "Volatility Measure" : "Covariance",
           "Jacobian" : False,
           "Optimization": "Differential Evolution",
           "Training Start": "2008-01-03",
           "Training End": "2011-06-30",
           "Historic Column": "Close",
           "Data Transformation": "Diff"}

def GeneratePortfolio():
    instruments = GetBenchmarkLargeCapConstituents()

    historicData = GetBenchmarkPriceMatrix(details["Training Start"], 
                                           details["Training End"], 
                                           details["Historic Column"])

    historicData = historicData[historicData.columns.intersection(instruments.index)]

    covMatrix = historicData.diff().cov()

    bounds = tuple([(0, 1) for x in range(covMatrix.shape[0])])

    minimizeFunction = lambda weights : NormalizedVariance(weights, covMatrix)

    return PostProcessResults(
        covMatrix.index.values,                                                 
        differential_evolution(func=minimizeFunction,
                               bounds=bounds,
                               disp=True,
                               polish=True),
        details)