from Utilities.ConstituentUtilities import GetBenchmarkConstituents
from Utilities.PortfolioUtilities import PostProcessResults
from Utilities.DataUtilities import GetBenchmarkPriceMatrix
from Utilities.PortfolioMeasurements import Variance, VarianceDerivative, Normalize, NonZeroMin
from scipy.optimize import basinhopping
import numpy as np


details = {"Constitiuents" : "Benchmark 100 Least Var",
           "Volatility Measure" : "Covariance",
           "Jacobian" : True,
           "Optimization": "Basinhopping + SLSQP + Constriants",
           "Starting Weight": "1/Vol",
           "Training Start": "2008-01-03",
           "Training End": "2011-06-30",
           "Historic Column": "Close",
           "Data Transformation": "Percentage Change"}

def GeneratePortfolio():
    instruments = GetBenchmarkConstituents()

    historicData = GetBenchmarkPriceMatrix(details["Training Start"], 
                                           details["Training End"], 
                                           details["Historic Column"])

    leastVolatile = historicData.var().sort_values().head(100)

    covMatrix = historicData[leastVolatile.index].pct_change().cov()

    minimizeFunction = lambda weights : Variance(weights, covMatrix)
    jacFunction = lambda weights : VarianceDerivative(weights, covMatrix)

    initialWeights = Normalize(np.array([1/leastVolatile[x] for x in covMatrix.index.values]))
    bounds = tuple([(0, 0.3) for x in range(covMatrix.shape[0])])

    return PostProcessResults(covMatrix.index.values,
                              basinhopping(func=minimizeFunction,
                              x0= initialWeights,
                              niter=500,
                              stepsize=5,
                              minimizer_kwargs = {
                                  "jac":jacFunction,
                                  "method":"SLSQP",
                                  "constraints": {'type': 'eq', 'fun': lambda x:  x.sum() - 1},                                  
                                  "options" : {
                                              "disp": False
                                          }, 
                                  "bounds": bounds},
                              disp=True
                              ),
                              details)

def NonZeroMinWrapper(x):

    if (NonZeroMin(x) > 0.1):
        return 0
    else:
        return 1