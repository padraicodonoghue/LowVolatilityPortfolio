from Utilities.ConstituentUtilities import GetBenchmarkLargeCapConstituents
from Utilities.PortfolioUtilities import PostProcessResults
from Utilities.DataUtilities import GetBenchmarkPriceMatrix
from Utilities.PortfolioMeasurements import NormalizedVariance, NormalizedVarianceDerivative, Normalize
from scipy.optimize import basinhopping
import numpy as np
import Utilities


details = {"Constitiuents" : "Benchmark Large Cap",
           "Volatility Measure" : "Covariance",
           "Jacobian" : True,
           "Optimization": "Basinhopping",
           "Starting Weight": "Equal Weight",
           "Training Start": "2008-01-03",
           "Training End": "2011-06-30",
           "Historic Column": "Close",
           "Data Transformation": "Percentage Change"}

def GeneratePortfolio():
    instruments = GetBenchmarkLargeCapConstituents()

    historicData = GetBenchmarkPriceMatrix(details["Training Start"], 
                                           details["Training End"], 
                                           details["Historic Column"])

    histroicData = historicData[historicData.columns.intersection(instruments.index)]

    covMatrix = historicData.pct_change().cov()

    minimizeFunction = lambda weights : NormalizedVariance(weights, covMatrix)
    jacFunction = lambda weights : NormalizedVarianceDerivative(weights, covMatrix)

    initialWeights = Normalize(np.array([1/covMatrix.shape[0] for x in covMatrix.index.values]))
    bounds = tuple([(0, None) for x in range(covMatrix.shape[0])])

    return PostProcessResults(covMatrix.index.values,
                              basinhopping(func=minimizeFunction,
                              x0= initialWeights,
                              minimizer_kwargs = {
                                  "jac":jacFunction,
                                  "method":"L-BFGS-B",
                                  "options" : {
                                              "disp": False
                                          }, 
                                  "bounds": bounds},
                              disp=True
                              ),
                              details)