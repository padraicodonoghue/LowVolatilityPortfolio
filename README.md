# Low Volatility Portfolio Generator

## Requirements
Python 3

Requirements detailed in Requirements.txt

## Running
1. Run Run.py to generate a Portfolio using default method
```
python3 Run.py
```

2. Using interpreter: Import Harness and a Module from Solutions/
```python
import Harness
from Solutions import BasinHoppingBenchmark
Harness.RunEvaluation(BasinHoppingBenchmark.GeneratePortfolio)
```

Results will be saved in ```Portfolios/{CurrentDateTime}/```